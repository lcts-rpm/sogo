%global sope_lib_version 4.9
%global sbjson_lib_version 2

Name: sope
Version: 4.0.4
Release: 3%{?dist}
# the entire source code is LGPLv2+
# except /sope-json/SBJson/* which is BSD
License: LGPLv2+ and BSD
Summary: A framework for developing web applications and services
URL: https://sogo.nu
Source: https://sogo.nu/files/downloads/SOGo/Sources/SOPE-%{version}.tar.gz
Patch0: python2.patch
BuildRequires: gcc gcc-objc
BuildRequires: gnustep-make gnustep-base gnustep-base-devel
BuildRequires: libxml2-devel openldap-devel postgresql-devel mariadb-devel
%description
SOPE provides ann extensive set of frameworks which form a complete Web
application server environment. Besides the Apple WebObjects compatible app
server extended with Zope concepts, it contains a large set of reusable
classes: XML processing (SAX, DOM, XML-RPC), MIME/IMAP4 processing,
LDAP connectivity, RDBMS connectivity, and iCalendar parsing.

#########################################
%package xml
Summary: SOPE libraries for XML processing
%description xml
These libraries contain:

  * a SAX2 Implementation for Objective-C
  * an attempt to implement DOM on top of SaxObjC
  * an XML-RPC implementation (without a transport layer)

SOPE is a framework for developing web applications and services. The
name "SOPE" (SKYRiX Object Publishing Environment) is inspired by ZOPE.

%package xml-devel
Summary: Development files for the SOPE XML libraries
Requires: %{name}-xml%{?_isa} = %{version}-%{release} libxml2-devel
%description xml-devel
This package contains the development files of the SOPE XML libraries.

SOPE is a framework for developing web applications and services. The
name "SOPE" (SKYRiX Object Publishing Environment) is inspired by ZOPE.

#########################################
%package sbjson
Summary: SBJson JSON library
%description sbjson
A high performance JSON library written in Objective-C.
This package contains the 2.x branch of this library, for compatability with
SOPE.

The project homepage can be found at: https://github.com/stig/json-framework

%package sbjson-devel
Summary: Development files for the SBJson library
Requires: %{name}-sbjson%{?_isa} = %{version}-%{release}
%description sbjson-devel
Development files for the SBJson library.

A high performance JSON library written in Objective-C.
This package contains the 2.x branch of this library, for compatability with
SOPE.

The project homepage can be found at: https://github.com/stig/json-framework

#########################################
%package core
Summary: Core libraries of the SOPE application server
Requires: %{name}-xml%{?_isa} = %{version}-%{release}
%description core
The SOPE core libraries contain:

  * various Foundation extensions
  * a java.io-like stream and socket library

SOPE is a framework for developing web applications and services. The
name "SOPE" (SKYRiX Object Publishing Environment) is inspired by ZOPE.

%package core-devel
Summary: Development files for the SOPE core libraries
Requires: %{name}-core%{?_isa} = %{version}-%{release}
%description core-devel
This package contains the header files for the SOPE core
libraries,  which are part of the SOPE application server framework.

SOPE is a framework for developing web applications and services. The
name "SOPE" (SKYRiX Object Publishing Environment) is inspired by ZOPE.

#########################################
%package mime
Summary: SOPE libraries for MIME processing
Requires: %{name}-core%{?_isa} = %{version}-%{release} %{name}-xml%{?_isa} = %{version}-%{release}
%description mime
The SOPE libraries for MIME processing contain:

  * classes for processing MIME entities
  * a full IMAP4 implementation
  * prototypical POP3 and SMTP processor

SOPE is a framework for developing web applications and services. The
name "SOPE" (SKYRiX Object Publishing Environment) is inspired by ZOPE.

%package mime-devel
Summary: Development files for the SOPE MIME libraries
Requires: %{name}-mime%{?_isa} = %{version}-%{release}
%description mime-devel
This package contains the development files of the SOPE
MIME libraries.

SOPE is a framework for developing web applications and services. The
name "SOPE" (SKYRiX Object Publishing Environment) is inspired by ZOPE.

#########################################
%package appserver
Summary: SOPE application server libraries
Requires: %{name}-xml%{?_isa} = %{version}-%{release} %{name}-core%{?_isa} = %{version}-%{release} %{name}-mime%{?_isa} = %{version}-%{release}
%description appserver
The SOPE application server libraries provide:

  * template rendering engine, lots of dynamic elements
  * HTTP client/server
  * XML-RPC client
  * WebDAV server framework
  * session management
  * scripting extensions for Foundation, JavaScript bridge
  * DOM tree rendering library

SOPE is a framework for developing web applications and services. The
name "SOPE" (SKYRiX Object Publishing Environment) is inspired by ZOPE.

%package appserver-devel
Summary: Development files for the SOPE application server libraries
Requires: %{name}-appserver%{?_isa} = %{version}-%{release}
%description appserver-devel
This package contains the development files for the SOPE application server
libraries.

SOPE is a framework for developing web applications and services. The
name "SOPE" (SKYRiX Object Publishing Environment) is inspired by ZOPE.

#########################################
%package ldap
Summary: SOPE libraries for LDAP access
Requires: %{name}-core%{?_isa} = %{version}-%{release} %{name}-xml%{?_isa} = %{version}-%{release}
%description ldap
The SOPE libraries for LDAP access contain an Objective-C wrapper for
LDAP directory services.

SOPE is a framework for developing web applications and services. The
name "SOPE" (SKYRiX Object Publishing Environment) is inspired by ZOPE.

%package ldap-devel
Summary: Development files for the SOPE LDAP libraries
Requires: %{name}-ldap%{?_isa} = %{version}-%{release}
%description ldap-devel
This package contains the development files of the SOPE
LDAP libraries.

SOPE is a framework for developing web applications and services. The
name "SOPE" (SKYRiX Object Publishing Environment) is inspired by ZOPE.

#########################################
%package gdl1
Summary: GNUstep database libraries for SOPE
Requires: %{name}-core%{?_isa} = %{version}-%{release} %{name}-xml%{?_isa} = %{version}-%{release}
%description gdl1
This package contains a fork of the GNUstep database libraries used
by the SOPE application server (excluding GDLContentStore).

SOPE is a framework for developing web applications and services. The
name "SOPE" (SKYRiX Object Publishing Environment) is inspired by ZOPE.

%package gdl1-postgresql
Summary: PostgreSQL connector for SOPE's fork of the GNUstep database environment
Requires: %{name}-gdl1%{?_isa} = %{version}-%{release} postgresql-libs
%description gdl1-postgresql
This package contains the PostgreSQL connector for SOPE's fork of the
GNUstep database libraries.

SOPE is a framework for developing web applications and services. The
name "SOPE" (SKYRiX Object Publishing Environment) is inspired by ZOPE.

%package gdl1-mysql
Summary: MySQL connector for SOPE's fork of the GNUstep database environment
Requires: %{name}-gdl1%{?_isa} = %{version}-%{release}
%description gdl1-mysql
This package contains the MySQL connector for SOPE's fork of the
GNUstep database libraries.

SOPE is a framework for developing web applications and services. The
name "SOPE" (SKYRiX Object Publishing Environment) is inspired by ZOPE.

%package gdl1-devel
Summary: Development files for the GNUstep database libraries
Requires: %{name}-gdl1%{?_isa} = %{version}-%{release}
%description gdl1-devel
This package contains the header files for SOPE's fork of the GNUstep
database libraries.

SOPE is a framework for developing web applications and services. The
name "SOPE" (SKYRiX Object Publishing Environment) is inspired by ZOPE.

########################################

%prep
%autosetup -p1 -n SOPE

# ****************************** build ********************************
%build
%set_build_flags
# grab config.(guess|sub) from system
if [ -f /usr/lib/rpm/redhat/config.sub ]
then
  cp /usr/lib/rpm/redhat/{config.sub,config.guess} sope-core/NGStreams/
elif [ -f /usr/lib/rpm/config.sub ]
then
  cp /usr/lib/rpm/{config.sub,config.guess} sope-core/NGStreams/
fi

# ./configure is called without --libdir, as GNUstep takes care of the
# environment
./configure  --disable-strip --with-gnustep --disable-debug
%{make_build}

# ****************************** install ******************************
%install
%{make_install} GNUSTEP_INSTALLATION_DOMAIN=SYSTEM

cp sope-mime/NGImap4/COPYING sope-mime/COPYING.NGImap4
cp sope-mime/NGImap4/README sope-mime/README.NGImap4
cp sope-mime/NGImap4/ChangeLog sope-mime/ChangeLog.NGImap4
cp sope-mime/NGMail/COPYING sope-mime/COPYING.NGMail
cp sope-mime/NGMail/README sope-mime/README.NGMail
cp sope-mime/NGMail/ChangeLog sope-mime/ChangeLog.NGMail
cp sope-mime/NGMime/COPYING sope-mime/COPYING.NGMime
cp sope-mime/NGMime/COPYRIGHT sope-mime/COPYRIGHT.NGMime
cp sope-mime/NGMime/README sope-mime/README.NGMime
cp sope-mime/NGMime/ChangeLog sope-mime/ChangeLog.NGMime

rm -f ${RPM_BUILD_ROOT}%{_bindir}/otest
rm -rf ${RPM_BUILD_ROOT}%{gnustep_libdir}/GDLAdaptors-*/SQLite3.gdladaptor

# ****************************** files ********************************
%files xml
%doc sope-xml/README sope-xml/Version sope-xml/ChangeLog
%license sope-xml/COPYING sope-xml/COPYRIGHT
%{_libdir}/libDOM*.so.%{sope_lib_version}*
%{_libdir}/libSaxObjC*.so.%{sope_lib_version}*
%{_libdir}/libXmlRpc*.so.%{sope_lib_version}*
%{gnustep_libdir}/SaxDrivers-*

%files xml-devel
%{_includedir}/DOM
%{_includedir}/SaxObjC
%{_includedir}/XmlRpc
%{_libdir}/libDOM*.so
%{_libdir}/libSaxObjC*.so
%{_libdir}/libXmlRpc*.so

%files sbjson
%doc sope-json/SBJson/Readme.markdown sope-json/SBJson/Installation.markdown
%doc sope-json/SBJson/Credits.markdown sope-json/SBJson/Changes.markdown
%doc sope-json/SBJson/Notes/parser-benchmark.txt sope-json/SBJson/Notes/JensAlfkePerformanceNotes.txt
%license sope-json/SBJson/LICENSE
%{_libdir}/libSBJson.so.%{sbjson_lib_version}*

%files sbjson-devel
%{_includedir}/SBJson
%{_libdir}/libSBJson.so

%files core
%doc sope-core/README sope-core/Version sope-core/ChangeLog
%license sope-core/COPYING sope-core/COPYRIGHT
%{_libdir}/libEOControl*.so.%{sope_lib_version}*
%{_libdir}/libNGExtensions*.so.%{sope_lib_version}*
%{_libdir}/libNGStreams*.so.%{sope_lib_version}*

%files core-devel
%{_includedir}/EOControl
%{_includedir}/NGExtensions
%{_includedir}/NGStreams
%{_libdir}/libEOControl*.so
%{_libdir}/libNGExtensions*.so
%{_libdir}/libNGStreams*.so

%files mime
%doc sope-mime/README* sope-mime/Version sope-mime/ChangeLog*
%license sope-mime/COPYING* sope-mime/COPYRIGHT*
%{_libdir}/libNGMime*.so.%{sope_lib_version}*

%files mime-devel
%{_includedir}/NGImap4
%{_includedir}/NGMail
%{_includedir}/NGMime
%{_libdir}/libNGMime*.so

%files appserver
%doc sope-appserver/Version sope-appserver/ChangeLog
%license sope-appserver/COPYING sope-appserver/COPYRIGHT
%{_libdir}/libNGObjWeb*.so.%{sope_lib_version}*
%{_libdir}/libWEExtensions*.so.%{sope_lib_version}*
%{_libdir}/libWOExtensions*.so.%{sope_lib_version}*
%{gnustep_libdir}/Libraries/Resources/NGObjWeb/*
%{gnustep_libdir}/SoProducts-*
%{gnustep_libdir}/WOxElemBuilders-*

%files appserver-devel
%{_bindir}/wod
%{_includedir}/NGHttp
%{_includedir}/NGObjWeb
%{_includedir}/WEExtensions
%{_includedir}/WOExtensions
%{_libdir}/libNGObjWeb*.so
%{_libdir}/libWEExtensions*.so
%{_libdir}/libWOExtensions*.so
%{gnustep_makefiles}

%files ldap
%doc sope-ldap/NGLdap/README sope-ldap/NGLdap/Version sope-ldap/NGLdap/ChangeLog
%license sope-ldap/NGLdap/COPYING sope-ldap/NGLdap/COPYRIGHT
%{_libdir}/libNGLdap*.so.%{sope_lib_version}*

%files ldap-devel
%{_includedir}/NGLdap
%{_libdir}/libNGLdap*.so

%files gdl1
%doc sope-gdl1/GDLAccess/README sope-gdl1/GDLAccess/Version sope-gdl1/GDLAccess/ChangeLog
%license sope-gdl1/GDLAccess/COPYING.LIB
%{_bindir}/connect-EOAdaptor
%{_bindir}/load-EOAdaptor
%{_libdir}/libGDLAccess*.so.%{sope_lib_version}*

%files gdl1-postgresql
%doc sope-gdl1/PostgreSQL/README sope-gdl1/PostgreSQL/Version sope-gdl1/PostgreSQL/ChangeLog
%license sope-gdl1/PostgreSQL/COPYING.LIB sope-gdl1/PostgreSQL/COPYING
%{gnustep_libdir}/GDLAdaptors-*/PostgreSQL.gdladaptor

%files gdl1-mysql
%doc sope-gdl1/MySQL/README sope-gdl1/MySQL/Version sope-gdl1/MySQL/ChangeLog
%license sope-gdl1/MySQL/COPYING.LIB
%{gnustep_libdir}/GDLAdaptors-*/MySQL.gdladaptor

%files gdl1-devel
%{_includedir}/GDLAccess
%{_libdir}/libGDLAccess*.so

# ********************************* changelog *************************
%changelog
* Thu Nov 15 2018 Christopher Engelhard <ce@lcts.de> 4.0.4-3
- add license and doc to packages

* Tue Nov 13 2018 Christopher Engelhard <ce@lcts.de> 4.0.4-2
- replace /usr/bin/(env python|python) with /usr/bin/python2
- updated source files
* Tue Nov 13 2018 Christopher Engelhard <ce@lcts.de> 4.0.4-1
- new package built with tito
- ported from Inverse's official specfile for RHEL
